FROM node:slim
COPY ./ /opt/host/
WORKDIR /opt/host
RUN npm install
ENTRYPOINT node server.js