const express = require('express');
const path = require('path');

const app = express();

// Serve static react files
app.use(express.static(path.join(__dirname, 'public')));

// Default everything else to index.html
app.get('*', (_req, res) => {
    res.sendFile(path.join(__dirname + '/public/index.html'));
});

const port = process.env.PORT || 80;
app.listen(port);

console.log('Listening on port ' + port);
